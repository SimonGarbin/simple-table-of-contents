<?php

/**
 * Plugin Name: Simple Table of Contents
 * Plugin URI: https://gitlab.com/SimonGarbin/simple-table-of-contents
 * Description: ...
 * Version: 1.0.0
 * Requires at least: 6.2
 * Requires PHP: 8.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: simple-table-of-contents
 * Domain Path: /languages

  Copyright 2023 Simon Garbin
  Simple Table of Contents is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Simple Table of Contents is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Simple Table of Contents. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
defined('ABSPATH') || exit;

/*
 * ----------------------------------------
 * Block registration and rendering
 * ----------------------------------------
 */

add_action('init', 'simple_table_of_contents_register_block');

function simple_table_of_contents_register_block() {

    $asset_file = include(plugin_dir_path(__FILE__) . '/block/block.asset.php');
    $script_handle = 'simple-table-of-contents-editor-script';

    wp_register_style(
            'simple-table-of-contents-style',
            plugins_url('block/style.css', __FILE__),
            array(),
            false,
            'all'
    );

    wp_register_style(
            'simple-table-of-contents-editor-style',
            plugins_url('block/editor.css', __FILE__),
            array(),
            false,
            'all'
    );

    wp_register_style(
            handle: 'font-awesome-v6.4.0',
            src: 'https://use.fontawesome.com/releases/v6.4.0/css/all.css',
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_register_script(
            handle: 'simple-table-of-contents-toggle-script',
            src: plugins_url('/assets/js/toggle.js', __FILE__),
            deps: array('jquery'),
            ver: false,
            in_footer: true
    );

    wp_register_script(
            $script_handle,
            plugins_url('block/block.js', __FILE__),
            $asset_file['dependencies'],
            $asset_file['version']
    );

    wp_set_script_translations(
            $script_handle,
            'simple-table-of-contents',
            plugin_dir_path(__FILE__) . 'languages/'
    );

    register_block_type(
            plugin_dir_path(__FILE__) . '/block/',
            array(
                'api_version' => 2,
                'render_callback' => 'simple_table_of_contents_render_callback',
            )
    );
}

function simple_table_of_contents_render_callback($attributes, $content, $block) {

    if (!isset($attributes['title']) || !$attributes['title']) {
        return '';
    }

    // Anchor and classes
    $anchor = '';

    if (isset($attributes['anchor']) && $attributes['anchor']) {
        $anchor .= sprintf(' id="%s"', esc_attr($attributes['anchor']));
    }

    $classes = array(
        'simple-table-of-contents',
    );

    if (!isset($attributes['openByDefault']) || !$attributes['openByDefault']) {
        $classes[] = 'is-closed';
    }

    if (isset($attributes['alwaysOpen']) && $attributes['alwaysOpen']) {
        $classes[] = 'always-open';
    }

    if (isset($attributes['bordered']) && $attributes['bordered']) {
        $classes[] = 'has-border';
    }

    if (isset($attributes['className']) && $attributes['className']) {
        $classes[] = $attributes['className'];
    }

    // Gather styles from block attributes
    $styles = array(
        'block' => array(),
        'title-wrap' => array(),
        'title' => array(),
        'table' => array(),
    );

    if (isset($attributes['width']) && $attributes['width']) {
        $styles['block'][] = 'width:' . esc_attr($attributes['width']);
    }

    if (isset($attributes['alignment']) && $attributes['alignment']) {
        if ($attributes['alignment'] === 'center') {
            $styles['block'][] = 'margin-left:auto';
            $styles['block'][] = 'margin-right:auto';
        } else if ($attributes['alignment'] === 'right') {
            $styles['block'][] = 'margin-left:auto';
            $styles['block'][] = 'margin-right:0';
        }
    }

    if (isset($attributes['bordered']) && $attributes['bordered']) {
        if (isset($attributes['titleBgColor']) && $attributes['titleBgColor']) {
            $styles['table'][] = 'border-color:' . esc_attr($attributes['titleBgColor']);
        }
    }

    if (isset($attributes['borderRadius']) && $attributes['borderRadius']) {
        $styles['block'][] = 'border-radius:' . esc_attr($attributes['borderRadius']);
        if ((!isset($attributes['bordered']) || !$attributes['bordered']) && !isset($attributes['contentBgColor'])) {
            $styles['title-wrap'][] = 'border-radius:' . esc_attr($attributes['borderRadius']);
        }
        $styles['table'][] = 'border-bottom-left-radius:' . esc_attr($attributes['borderRadius']);
        $styles['table'][] = 'border-bottom-right-radius:' . esc_attr($attributes['borderRadius']);
    }

    if (isset($attributes['titleFontColor']) && $attributes['titleFontColor']) {
        $styles['title'][] = 'color:' . esc_attr($attributes['titleFontColor']);
    }

    if (isset($attributes['titleBgColor']) && $attributes['titleBgColor']) {
        $styles['title-wrap'][] = 'background:' . esc_attr($attributes['titleBgColor']);
        $styles['title-wrap'][] = 'border-color:' . esc_attr($attributes['titleBgColor']);
    }

    if (isset($attributes['contentFontColor']) && $attributes['contentFontColor']) {
        $styles['table'][] = 'color:' . esc_attr($attributes['contentFontColor']);
    }

    if (isset($attributes['contentBgColor']) && $attributes['contentBgColor']) {
        $styles['table'][] = 'background:' . esc_attr($attributes['contentBgColor']);
    }

    if (isset($attributes['openByDefault']) && !$attributes['openByDefault']) {
        $styles['table'][] = 'display:none';
    }

    // Combine CSS properties to style attribute
    foreach ($styles as $key => $values) {
        if (!empty($values)) {
            $styles[$key] = sprintf(' style="%s;"', implode(';', $values));
        } else {
            $styles[$key] = '';
        }
    }

    // Generate HTML
    $output = sprintf(
            '<div%1$s class="%2$s"%3$s>',
            $anchor,
            esc_attr(implode(' ', $classes)),
            $styles['block'],
    );

    // Title
    $no_pointer = '';
    if (isset($attributes['alwaysOpen']) && $attributes['alwaysOpen']) {
        $no_pointer = ' no-pointer';
    }
    $output .= sprintf(
            '<div class="simple-table-of-contents-title-wrap%2$s"%1$s>',
            $styles['title-wrap'],
            $no_pointer,
    );

    $title_tag = 'span';
    if (isset($attributes['titleTag']) && $attributes['titleTag']) {
        $title_tag = $attributes['titleTag'];
    }
    $output .= sprintf(
            '<%2$s class="simple-table-of-contents-title"%3$s>%1$s</%2$s>',
            $attributes['title'],
            $title_tag,
            $styles['title']
    );

    if (!isset($attributes['alwaysOpen']) || !$attributes['alwaysOpen']) {
        $output .= sprintf(
                        '<div class="simple-table-of-contents-arrow"%s>',
                        $styles['title']
                )
                . '<i class="fa fa-angle-down fa-sm"></i>'
                . '</div>';
    }
    $output .= '</div>';

    // Table of contents
    if (isset($attributes['headingTags']) && $attributes['headingTags']) {
        $tags = array();
        foreach ($attributes['headingTags'] as $tag => $value) {
            if ($value) {
                $tags[] = substr($tag, 1, 1);
            }
        }
    } else {
        $tags = array('h1', 'h2', 'h3');
    }
    $content = simple_table_of_contents_add_heading_ids(get_the_content());
    $headings = simple_table_of_contents_get_headings($content, $tags);

    $output .= sprintf(
            '<div class="simple-table-of-contents-table"%1$s>%2$s</div>',
            $styles['table'],
            simple_table_of_contents_generate_table($headings),
    );

    $output .= '</div>';

    return $output;
}

function simple_table_of_contents_generate_table($headings) {

    $toc = '';

    for ($i = 0; $i <= count($headings); $i++) {

        $previous = $i > 0 ? $headings[$i - 1] : null;
        $current = isset($headings[$i]) ? $headings[$i] : null;
        $next = null;
        if ($i < count($headings) && isset($headings[$i + 1])) {
            $next = $headings[$i + 1];
        }

        if ($current == null) {
            $toc .= '</li>';
            $toc .= '</ol>';
            break;
        }

        $level = $current['level'];
        $id = $current['id'];
        $text = $current['text'];

        if (!$previous) {
            $toc .= '<ol>';
        }

        if ($previous && $previous['level'] < $level) {
            for ($k = 0; $k < $level - $previous['level']; $k++) {
                $toc .= '<ol>';
            }
        }

        $toc .= sprintf('<li><a href="#%1$s">%2$s</a>', $id, $text);
        if ($next && $next['level'] > $level) {
            continue;
        }
        $toc .= '</li>';

        if ($next && $next['level'] == $level) {
            continue;
        }

        if (!$next || ($next && $next['level'] < $level)) {
            $toc .= '</ol>';
            if ($next && ($level - $next['level'] >= 2)) {
                $toc .= '</li>';
                for ($k = 1; $k < $level - $next['level']; $k++) {
                    $toc .= '</ol>';
                }
            }
        }
    }

    return $toc;
}

function simple_table_of_contents_get_headings($content, $tags) {

    $matches = array();
    $pattern = sprintf(
            '/<h(%1$s)([^<]*)>(.*)<\/h(%1$s)>/',
            implode('|', $tags),
    );
    preg_match_all($pattern, $content, $matches);

    $headings = array();

    for ($i = 0; $i < count($matches[1]); $i++) {

        $headings[$i]['level'] = intval($matches[1][$i]);
        $headings[$i]['text'] = $matches[3][$i];

        $id_matches = array();
        $id_pattern = '/id=\"([^\"]*)\"/';
        preg_match($id_pattern, $matches[2][$i], $id_matches);

        $headings[$i]['id'] = $id_matches[1];
    }

    return $headings;
}

/*
 * ----------------------------------------
 * Addition of heading IDs
 * ----------------------------------------
 */

add_filter('the_content', 'simple_table_of_contents_add_heading_ids');

function simple_table_of_contents_add_heading_ids($content) {

    $content = preg_replace_callback(
            '/<h([1-6])([^<]*)>(.*)<\/h[1-6]>/',
            'simple_table_of_contents_add_heading_ids_cb',
            $content,
    );

    return $content;
}

function simple_table_of_contents_add_heading_ids_cb($matches) {

    $level = $matches[1];
    $attributes = $matches[2];
    $text = $matches[3];
    $slug = simple_table_of_contents_sanitize_title($text);

    $id_match = array();
    $id_pattern = '/id=\"([^\"]*)\"/';
    preg_match($id_pattern, $attributes, $id_match);
    $id = isset($id_match[1]) ? $id_match[1] : null;

    if ($id) {
        $attributes = str_replace($id_match[0], '', $attributes);
    }

    $heading = sprintf(
            '<h%1$s id="%2$s"%3$s>%4$s</h%1$s>',
            $level,
            $id ?: $slug,
            $attributes ? ' ' . $attributes : '',
            $text,
    );

    return $heading;
}

function simple_table_of_contents_sanitize_title($title) {

    $title = str_replace(
            array('ä', 'ö', 'ü', 'ß'),
            array('ae', 'oe', 'ue', 'ss'),
            strtolower(strip_tags($title)),
    );
    $title = iconv('UTF-8', 'US-ASCII//TRANSLIT', $title);
    $title = sanitize_title_with_dashes($title);

    return $title;
}

/*
 * ----------------------------------------
 * Translation
 * ----------------------------------------
 */

add_action('init', 'simple_table_of_contents_load_textdomain');

function simple_table_of_contents_load_textdomain() {
    load_plugin_textdomain(
            'simple-table-of-contents',
            false,
            dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

function simple_table_of_contents_translation_dummy() {
    $plugin_description = __('...', 'simple-table-of-contents');
}
