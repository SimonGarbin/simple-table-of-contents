jQuery(document).ready(function ($) {

    $('.simple-table-of-contents:not(.always-open) .simple-table-of-contents-title-wrap').on(
            'click', function () {
                var titleWrap = $(this);
                var parent = titleWrap.parent();
                var content = titleWrap.next();

                parent.toggleClass('is-closed');
                if (parent.hasClass('is-closed')) {
                    content.slideUp(300);
                } else {
                    content.slideDown(300);
                }
            });
});