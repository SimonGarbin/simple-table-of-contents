(function (blocks, element, blockEditor, components, i18n) {

    var registerBlockType = blocks.registerBlockType;
    var el = element.createElement;
    var __ = i18n.__;

    var useBlockProps = blockEditor.useBlockProps;
    var InspectorControls = blockEditor.InspectorControls;
    var RichText = blockEditor.RichText;
    var PanelColorSettings = blockEditor.PanelColorSettings;

    var PanelBody = components.PanelBody;
    var BaseControl = components.BaseControl;
    var ToggleControl = components.ToggleControl;
    var SelectControl = components.SelectControl;
    var CheckboxControl = components.CheckboxControl;
    var UnitControl = components.__experimentalUnitControl;

    registerBlockType('simple-table-of-contents/block', {
        apiVersion: 2,
        title: __('Simple table of contents', 'simple-table-of-contents'),
        category: 'widgets',
        icon: 'editor-ul',
        keywords: [
            'table-of-contents',
            'table',
            'toc',
            'headings'
        ],
        supports: {
            anchor: true
        },
        attributes: {
            headingTags: {
                type: 'object',
                default: {
                    h1: true,
                    h2: true,
                    h3: true,
                    h4: false,
                    h5: false,
                    h6: false
                }
            },
            title: {
                type: 'string',
                default: null
            },
            titleTag: {
                type: 'string',
                default: ''
            },
            openByDefault: {
                type: 'boolean',
                default: false
            },
            alwaysOpen: {
                type: 'boolean',
                default: false
            },
            width: {
                type: 'string',
                default: null
            },
            alignment: {
                type: 'string',
                default: 'left'
            },
            bordered: {
                type: 'boolean',
                default: false
            },
            borderRadius: {
                type: 'string',
                default: null
            },
            titleFontColor: {
                type: 'string',
                default: null
            },
            titleBgColor: {
                type: 'string',
                default: null
            },
            contentFontColor: {
                type: 'string',
                default: null
            },
            contentBgColor: {
                type: 'string',
                default: null
            },
            anchor: {
                type: 'string',
                default: null
            }
        },
        edit: function (props) {
            var attributes = props.attributes;
            var setAttributes = props.setAttributes;

            function onChangeHeadingTags(newValue, level) {
                var newHeadingTags = attributes.headingTags;
                newHeadingTags[level] = newValue;
                setAttributes({headingTags: {
                        h1: newHeadingTags['h1'],
                        h2: newHeadingTags['h2'],
                        h3: newHeadingTags['h3'],
                        h4: newHeadingTags['h4'],
                        h5: newHeadingTags['h5'],
                        h6: newHeadingTags['h6']
                    }
                });
            }

            function onChangeTitle(newTitle) {
                setAttributes({title: newTitle || null});
            }

            function onChangeTitleTag(newTitleTag) {
                setAttributes({titleTag: newTitleTag});
            }

            function onChangeWidth(newWidth) {
                setAttributes({width: newWidth || null});
            }

            function onChangeBorderRadius(newBorderRadius) {
                setAttributes({borderRadius: newBorderRadius || null});
            }

            function onChangeTitleFontColor(newTitleFontColor) {
                setAttributes({titleFontColor: newTitleFontColor || null});
            }

            function onChangeTitleBgColor(newTitleBgColor) {
                setAttributes({titleBgColor: newTitleBgColor || null});
            }

            function onChangeContentFontColor(newContentFontColor) {
                setAttributes({contentFontColor: newContentFontColor || null});
            }

            function onChangeContentBgColor(newContentBgColor) {
                setAttributes({contentBgColor: newContentBgColor || null});
            }

            function onChangeOpenByDefault(newOpenByDefault) {
                if (attributes.alwaysOpen) {
                    setAttributes({openByDefault: true});
                } else {
                    setAttributes({openByDefault: newOpenByDefault});
                }
            }

            function onChangeAlwaysOpen(newAlwaysOpen) {
                setAttributes({alwaysOpen: newAlwaysOpen});
                if (newAlwaysOpen) {
                    setAttributes({openByDefault: true});
                }
            }

            function onChangeBordered(newBordered) {
                setAttributes({bordered: newBordered});
            }

            function onChangeAlignment(newAlignment) {
                setAttributes({alignment: newAlignment});
            }

            function getMargin() {
                if (attributes.alignment === 'left') {
                    return '1em auto 1em 0';
                } else if (attributes.alignment === 'center') {
                    return '1em auto';
                } else if (attributes.alignment === 'right') {
                    return '1em 0 1em auto';
                }
            }

            return el('div', useBlockProps(),
                    el('div', {
                        id: attributes.anchor,
                        className: 'simple-table-of-contents is-closed',
                        style: {
                            'width': attributes.width,
                            'margin': getMargin(),
                            'border-radius': attributes.borderRadius
                        }
                    },
                            el('div', {
                                className: 'simple-table-of-contents-title-wrap',
                                style: {
                                    'background': attributes.titleBgColor,
                                    'border-color': attributes.titleBgColor
                                            ? attributes.titleBgColor : null
                                }
                            },
                                    el(RichText, {
                                        tagName: 'span',
                                        className: 'simple-table-of-contents-title',
                                        value: attributes.title,
                                        placeholder: __('Table of contents', 'simple-table-of-contents'),
                                        onChange: onChangeTitle,
                                        style: {
                                            'color': attributes.titleFontColor
                                        }
                                    }),
                                    el('div', {
                                        className: 'simple-table-of-contents-arrow',
                                        style: {
                                            'color': attributes.titleFontColor
                                        }
                                    },
                                            el('i', {className: 'fa fa-angle-down fa-sm'})
                                            )
                                    )
                            ),
                    el(InspectorControls, {group: 'settings'},
                            el(PanelBody, {},
                                    el(BaseControl, {
                                        label: __('Included headings', 'simple-table-of-contents'),
                                        className: 'simple-table-of-contents-heading-tags-control'
                                    },
                                            el(CheckboxControl, {
                                                label: 'h1',
                                                checked: attributes.headingTags['h1'],
                                                onChange: function (val) {
                                                    onChangeHeadingTags(val, 'h1');
                                                }
                                            }),
                                            el(CheckboxControl, {
                                                label: 'h2',
                                                checked: attributes.headingTags['h2'],
                                                onChange: function (val) {
                                                    onChangeHeadingTags(val, 'h2');
                                                }
                                            }),
                                            el(CheckboxControl, {
                                                label: 'h3',
                                                checked: attributes.headingTags['h3'],
                                                onChange: function (val) {
                                                    onChangeHeadingTags(val, 'h3');
                                                }
                                            }),
                                            el(CheckboxControl, {
                                                label: 'h4',
                                                checked: attributes.headingTags.h4,
                                                onChange: function (val) {
                                                    onChangeHeadingTags(val, 'h4');
                                                }
                                            }),
                                            el(CheckboxControl, {
                                                label: 'h5',
                                                checked: attributes.headingTags.h5,
                                                onChange: function (val) {
                                                    onChangeHeadingTags(val, 'h5');
                                                }
                                            }),
                                            el(CheckboxControl, {
                                                label: 'h6',
                                                checked: attributes.headingTags.h6,
                                                onChange: function (val) {
                                                    onChangeHeadingTags(val, 'h6');
                                                }
                                            })
                                            ),
                                    el(SelectControl, {
                                        label: __('Title tag', 'simple-table-of-contents'),
                                        labelPosition: 'side',
                                        className: 'simple-table-of-contents-input-control',
                                        value: attributes.titleTag,
                                        onChange: onChangeTitleTag,
                                        options: [
                                            {value: 'span', label: 'span', selected: true},
                                            {value: 'p', label: 'p'},
                                            {value: 'h1', label: 'h1'},
                                            {value: 'h2', label: 'h2'},
                                            {value: 'h3', label: 'h3'},
                                            {value: 'h4', label: 'h4'}
                                        ]
                                    }),
                                    el(ToggleControl, {
                                        checked: attributes.openByDefault,
                                        label: __('Open by default', 'simple-table-of-contents'),
                                        onChange: onChangeOpenByDefault,
                                        disabled: attributes.alwaysOpen
                                    }),
                                    el(ToggleControl, {
                                        checked: attributes.alwaysOpen,
                                        label: __('Always open', 'simple-table-of-contents'),
                                        onChange: onChangeAlwaysOpen
                                    }),
                                    )
                            ),
                    el(InspectorControls, {group: 'styles'},
                            el(PanelBody, {},
                                    el(UnitControl, {
                                        label: __('Width', 'simple-table-of-contents'),
                                        labelPosition: 'side',
                                        className: 'simple-table-of-contents-input-control',
                                        value: attributes.width,
                                        onChange: onChangeWidth
                                    }),
                                    el(SelectControl, {
                                        label: __('Alignment', 'simple-table-of-contents'),
                                        labelPosition: 'side',
                                        className: 'simple-table-of-contents-input-control',
                                        value: attributes.alignment,
                                        onChange: onChangeAlignment,
                                        options: [
                                            {
                                                value: 'left',
                                                label: __('left', 'simple-table-of-contents'),
                                                selected: true
                                            },
                                            {
                                                value: 'center',
                                                label: __('center', 'simple-table-of-contents')
                                            },
                                            {
                                                value: 'right',
                                                label: __('right', 'simple-table-of-contents')
                                            }
                                        ]
                                    }),
                                    el(UnitControl, {
                                        label: __('Corner radius', 'simple-table-of-contents'),
                                        labelPosition: 'side',
                                        className: 'simple-table-of-contents-input-control',
                                        value: attributes.borderRadius,
                                        onChange: onChangeBorderRadius
                                    }),
                                    el(ToggleControl, {
                                        checked: attributes.bordered,
                                        label: __('Border', 'simple-table-of-contents'),
                                        onChange: onChangeBordered,
                                        id: 'hallo'
                                    })
                                    ),
                            el(PanelColorSettings, {
                                title: __('Title colors', 'simple-table-of-contents'),
                                colorSettings: [
                                    {
                                        value: attributes.titleFontColor,
                                        onChange: onChangeTitleFontColor,
                                        label: __('Text', 'simple-table-of-contents')
                                    },
                                    {
                                        value: attributes.titleBgColor,
                                        onChange: onChangeTitleBgColor,
                                        label: __('Background', 'simple-table-of-contents')
                                    }
                                ]
                            }),
                            el(PanelColorSettings, {
                                title: __('Content colors', 'simple-table-of-contents'),
                                colorSettings: [
                                    {
                                        value: attributes.contentFontColor,
                                        onChange: onChangeContentFontColor,
                                        label: __('Text', 'simple-table-of-contents')
                                    },
                                    {
                                        value: attributes.contentBgColor,
                                        onChange: onChangeContentBgColor,
                                        label: __('Background', 'simple-table-of-contents')
                                    }
                                ]
                            })
                            )
                    );
        },
        save: function (props) {
            return;
        }
    });
})(
        window.wp.blocks,
        window.wp.element,
        window.wp.blockEditor,
        window.wp.components,
        window.wp.i18n
        );